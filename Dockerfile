FROM registry.fedoraproject.org/fedora-toolbox:39 as chapel-base

RUN dnf -y install \
    bash \
    ca-certificates \
    clang \
    cmake \
    curl \
    file \
    git \
    clang-devel \
    clang-libs \
    clang-tools-extra \
    gmp-devel \
    llvm-devel \
    llvm \
    make \
    mawk \
    m4 \
    perl \
    pkg-config \
    protobuf-compiler \
    python-setuptools \
    python3 \
    python3-pip \
    python3-devel \
    wget \
    blas-devel lapack-devel \
    python3-numpy \
    zlib-devel \
    # Stuff for compiling wlroots
    meson \
    wayland-devel \
    wayland-protocols-devel \
    vulkan-loader-devel \
    vulkan-utility-libraries-devel \
    libdrm-devel \
    glslang-devel \
    mesa-libgbm-devel \
    libinput-devel \
    libxkbcommon-devel \
    systemd-devel \
    pixman-devel \
    libseat-devel \
    hwdata-devel \
    libdisplay-info-devel \
    libliftoff-devel \
    xorg-x11-server-Xwayland-devel \
    libxcb-devel

# Configure dummy git user
RUN git config --global user.email "noreply@example.com" && \
    git config --global user.name  "Chapel user"

ENV CHPL_VERSION=2.0.0
ENV CHPL_HOME=/opt/chapel/
ENV CHPL_GMP=system
ENV CHPL_LLVM=system
ENV CHPL_TASKS=qthreads

WORKDIR $CHPL_HOME

# Build stage!

FROM chapel-base as chapel-build

RUN mkdir -p /opt/chapel \
    && wget -q -O - https://github.com/chapel-lang/chapel/releases/download/$CHPL_VERSION/chapel-$CHPL_VERSION.tar.gz | tar -xzC /opt/chapel --transform 's/chapel-//' && mv /opt/chapel/$CHPL_VERSION/* /opt/chapel/

ENV CHPL_TARGET_COMPILER=llvm
# ENV CHPL_LLVM_CONFIG=/usr/bin/llvm-config-15

RUN CHPL_TARGET_COMPILER=llvm make -j8 \
    && CHPL_TARGET_COMPILER=gnu make -j8 \
    && env CHPL_TARGET_COMPILER=llvm CHPL_TASKS=fifo make -j8 \
    && env CHPL_TARGET_COMPILER=gnu CHPL_TASKS=fifo make -j8 \
    && make chpldoc test-venv mason -j8 \
    && make chapel-py-venv chplcheck chpl-language-server -j8 \
    && make cleanall

RUN cd $CHPL_HOME/bin && ln -s */* .

#ENV PATH $PATH:$CHPL_HOME/bin/linux64-x86_64:$CHPL_HOME/util
ENV PATH="${PATH}:${CHPL_HOME}/bin:${CHPL_HOME}/util"

FROM chapel-build as wlroots-build

ENV WLROOTS_RELEASE=0.17.3
RUN mkdir -p /build && cd /build && \
    wget https://gitlab.freedesktop.org/wlroots/wlroots/-/archive/0.17.3/wlroots-$WLROOTS_RELEASE.tar.gz && \
    tar xvf wlroots-$WLROOTS_RELEASE.tar.gz

RUN cd /build/wlroots-$WLROOTS_RELEASE && \
    meson setup build/ && \
    ninja -C build/ && \
    ninja -C build/ install

FROM wlroots-build as chapel-edit

RUN dnf -y install \
    neovim \
    vim \
    zsh \
    fish

RUN touch /yay
