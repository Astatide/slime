module wlroots {
  public use CTypes;
  extern {
    #include <assert.h>
    #include <getopt.h>
    #include <stdbool.h>
    #include <stdlib.h>
    #include <stdio.h>
    #include <time.h>
    #include <unistd.h>
    #include <wayland-server-core.h>
    #include <wlr/backend.h>
    #include <wlr/render/allocator.h>
    #include <wlr/render/wlr_renderer.h>
    #include <wlr/types/wlr_cursor.h>
    #include <wlr/types/wlr_compositor.h>
    #include <wlr/types/wlr_data_device.h>
    #include <wlr/types/wlr_input_device.h>
    #include <wlr/types/wlr_keyboard.h>
    #include <wlr/types/wlr_output.h>
    #include <wlr/types/wlr_output_layout.h>
    #include <wlr/types/wlr_pointer.h>
    #include <wlr/types/wlr_scene.h>
    #include <wlr/types/wlr_seat.h>
    #include <wlr/types/wlr_subcompositor.h>
    #include <wlr/types/wlr_xcursor_manager.h>
    #include <wlr/types/wlr_xdg_shell.h>
    #include <wlr/interfaces/wlr_buffer.h>
    #include <wlr/util/log.h>
    #include <xkbcommon/xkbcommon.h>
    #include <wlr/types/wlr_linux_dmabuf_v1.h>
    /* For brevity's sake, struct members are annotated where they are used. */
    //static int foo(int x) { return x + 1; }

    #include <assert.h>
    #include <drm_fourcc.h>
    #include <fcntl.h>
    #include <stdlib.h>
    #include <sys/mman.h>
    #include <unistd.h>
    #include <wlr/backend.h>
    #include <wlr/interfaces/wlr_buffer.h>
    #include <wlr/render/wlr_renderer.h>
    #include <wlr/types/wlr_compositor.h>
    #include <wlr/types/wlr_linux_dmabuf_v1.h>
    #include <wlr/types/wlr_output_layer.h>
    #include <wlr/util/log.h>
    #include <xf86drm.h>
    #include "linux-dmabuf-unstable-v1-protocol.h"
    #include <wlr/render/drm_format_set.h>

    struct wlr_linux_dmabuf_feedback_v1_compiled_tranche {
      dev_t target_device;
      uint32_t flags; // bitfield of enum zwp_linux_dmabuf_feedback_v1_tranche_flags
      struct wl_array indices; // uint16_t
    };

    struct wlr_linux_dmabuf_feedback_v1_compiled {
      dev_t main_device;
      int table_fd;
      size_t table_size;

      size_t tranches_len;
      struct wlr_linux_dmabuf_feedback_v1_compiled_tranche tranches[];
    };
  }
}

//writeln(WLRoots.foo(3));
//enum tinywl_cursor_mode {A, B}
//writeln(new WLRoots.tinywl_server());

