module Slime {
  public import sleep as Sleep;
  public import wlroots as wlr;

  enum tinywl_cursor_mode {
    TINYWL_CURSOR_PASSTHROUGH,
    TINYWL_CURSOR_MOVE,
    TINYWL_CURSOR_RESIZE,
  }

  class tinywl_server {
    //var wl_display: wlr.wl_display; // CAUSES ERROR
    var backend: wlr.wlr_backend;
    var renderer: wlr.wlr_renderer;
    var allocater: wlr.wlr_allocator;
    var scene: wlr.wlr_scene;
    //var scene_layout: wlr.wlr_scene_output_layout; // CAUSES ERROR
    var xdg_shell: wlr.wlr_xdg_shell;
    var new_xdg_surface: wlr.wl_listener;
    var toplevels: wlr.wl_list;

    var cursor: wlr.wlr_cursor;
    var cursor_mgr: wlr.wlr_xcursor_manager;
    var cursor_motion: wlr.wl_listener;
    var cursor_motion_absolute: wlr.wl_listener;
    var cursor_button: wlr.wl_listener;
    var cursor_axis: wlr.wl_listener;
    var cursor_frame: wlr.wl_listener;

    var seat: wlr.wlr_seat;
    var new_input: wlr.wl_listener;
    var request_cursor: wlr.wl_listener;
    var request_set_selection: wlr.wl_listener;
    var keyboards: wlr.wl_list;
    var cursor_mode: tinywl_cursor_mode;
    //var grabbed_toplevel: tinywl_toplevel;
    var grab_x: real;
    var grab_y: real;
    var grab_geobox: wlr.wlr_box;
    var resize_edges: int;
    var output_layout: wlr.wlr_output_layout;
    var outputs: wlr.wl_list;
    var new_output: wlr.wl_listener;

  }

  record tinywl_toplevel {
    var link: wlr.wl_list;
    //var server: borrowed tinywl_server;
    var xdg_toplevel: wlr.wlr_xdg_toplevel;
    var scene_tree: wlr.wlr_scene_tree;
    var map: wlr.wl_listener;
    var unmap: wlr.wl_listener;
    var commit: wlr.wl_listener;
    var destroy: wlr.wl_listener;
    var request_move: wlr.wl_listener;
    var request_resize: wlr.wl_listener;
    var request_maximize: wlr.wl_listener;
    var request_fullscreen: wlr.wl_listener;
  }

  record tinywl_output {
    var link: wlr.wl_list;
    //var server: tinywl_server; // putting a borrowed here results in a compiler error: internal error: LLV-CLA-TIL-4127 chpl version 2.0.0 ALSO temporarily commenting out because this needs an initialization and I don't care yet
    //var wlr_output: wlr.wlr_output; // errors out with  error: Could not find C type for wlr_swapchain
    var frame: wlr.wl_listener;
    var request_state: wlr.wl_listener;
    var destroy: wlr.wl_listener;
  };
}
