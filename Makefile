WAYLAND_PROTOCOLS=$(shell pkg-config --variable=pkgdatadir wayland-protocols)
WAYLAND_SCANNER=$(shell pkg-config --variable=wayland_scanner wayland-scanner)
CINC=\
	 $(shell pkg-config --cflags wlroots) \
	 $(shell pkg-config --cflags wayland-server) \
	 $(shell pkg-config --cflags xkbcommon)

LIBS=\
	 $(shell pkg-config --libs wlroots) \
	 $(shell pkg-config --libs wayland-server) \
	 $(shell pkg-config --libs xkbcommon)
WLROOTSLIB = $(shell pkg-config --libs wlroots)
export CHPL_TARGET_COMPILER = llvm
export CHPL_LLVM = system
export CHPL_TASKS = qthreads

# wayland-scanner is a tool which generates C headers and rigging for Wayland
# protocols, which are specified in XML. wlroots requires you to rig these up
# to your build system yourself and provide them in the include path.
xdg-shell-protocol.h:
	$(WAYLAND_SCANNER) server-header \
		$(WAYLAND_PROTOCOLS)/stable/xdg-shell/xdg-shell.xml $@

linux-dmabuf-unstable-v1-protocol.h:
	$(WAYLAND_SCANNER) server-header \
		$(WAYLAND_PROTOCOLS)/unstable/linux-dmabuf/linux-dmabuf-unstable-v1.xml $@

slime: src/slime.chpl xdg-shell-protocol.h linux-dmabuf-unstable-v1-protocol.h
	chpl -o $@ main.chpl src/slime.chpl --target-compiler=llvm --ccflags "  -I. -g -Isrc/ -DWLR_USE_UNSTABLE $(CINC)" --ldflags "$(LIBS)"

clean:
	rm -f slime xdg-shell-protocol.h xdg-shell-protocol.c linux-dmabuf-unstable-v1-protocol.h

.DEFAULT_GOAL=slime
.PHONY: clean

